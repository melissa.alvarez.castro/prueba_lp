# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-

##############################################################################
#
# Autor Melissa Alvarez Castro
# Prueba Longport Colombia
#
##############################################################################

{
    'name': 'Modulo de hijos',
    'version': '1.0',
    'author': 'Melissa Alvarez Castro',
    'depends': ['hr'],
    'description': """
        - Modulo que permite gestionar hijos de un empleado
    """,
    'data': [
        'views/employee_view.xml',
        'views/children_view.xml',
    ],
    'auto_install': False,
    'installable': True,
}