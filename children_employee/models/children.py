# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-

##############################################################################
#
# Autor Melissa Alvarez Castro
# Prueba Longport Colombia
#
##############################################################################

from openerp import models, fields, api
from datetime import datetime
from openerp.exceptions import Warning, ValidationError

opciones =[('pree', u"Preescolar"),
           ('prim', u"Primaria"),
           ('bach', u"Bachiller"),
           ('prof', u"Profesional"),
           ('ning', u"Ninguno")]

class children(models.Model):
    _name = 'employee.children'
    _description = 'Hijo de empleado'

    name = fields.Char(string=u"Nombre")
    num_doc = fields.Char(string=u"Número de documento")
    escolaridad = fields.Selection(string=u"Escolaridad", selection=opciones)
    f_nacimiento = fields.Date(string=u"Fecha nacimiento")
    edad = fields.Integer(string=u"Edad", compute='calcular_edad')

    employee_id = fields.Many2one(string=u"Empleado", comodel_name='hr.employee', required='True')

    @api.one
    @api.depends('f_nacimiento')
    def calcular_edad(self):
        """
        Campo edad calculdado a partir del campo f_nacimiento
        """
        if self.f_nacimiento:
            now = datetime.now().strftime('%Y-%m-%d')
            res = datetime.strptime(now, "%Y-%m-%d") - datetime.strptime(str(self.f_nacimiento), "%Y-%m-%d")
            self.edad = res.days / 365

    @api.constrains('num_doc')
    def validate_num_doc(self):
        """
        Validacion campo num_doc debe de ser unico
        """
        if self.num_doc:
            records = self.env['employee.children'].search([('num_doc', '=', self.num_doc)])
            if len(records) > 1:
                raise Warning(u"El número de documento del HIJO debe de ser ÚNICO, inténtelo nuevamente")
