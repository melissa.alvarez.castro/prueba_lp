# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-

##############################################################################
#
# Autor Melissa Alvarez Castro
# Prueba Longport Colombia
#
##############################################################################

from openerp import models, fields, api

class hremployee(models.Model):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    children_ids = fields.One2many(string=u"Hijos", comodel_name='employee.children', inverse_name='employee_id')