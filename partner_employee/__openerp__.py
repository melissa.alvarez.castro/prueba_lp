# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-

##############################################################################
#
# Autor Melissa Alvarez Castro
# Prueba Longport Colombia
#
##############################################################################

{
    'name': 'Modulo de terceros',
    'version': '1.0',
    'author': 'Melissa Alvarez Castro',
    'depends': ['hr'],
    'description': """
        - Modulo que permite gestionar el tercero de un empleado
    """,
    'data': [
        'views/employee_view.xml',
        'views/partner_view.xml',
    ],
    'auto_install': False,
    'installable': True,
}