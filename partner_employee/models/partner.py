# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-

##############################################################################
#
# Autor Melissa Alvarez Castro
# Prueba Longport Colombia
#
##############################################################################

from openerp import models, fields, api

class respartner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    num_id = fields.Char(string=u"Identificación")