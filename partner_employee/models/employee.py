# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-

##############################################################################
#
# Autor Melissa Alvarez Castro
# Prueba Longport Colombia
#
##############################################################################

from openerp import models, fields, api

class hremployee(models.Model):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    name_partner = fields.Char(string=u"Nombre", related='address_home_id.name')
    num_id_partner = fields.Char(string=u"Identificación", related='address_home_id.num_id')
    phone_partner = fields.Char(string=u"Telefono", related='address_home_id.phone')
    email_partner = fields.Char(string=u"Correo", related='address_home_id.email')

    @api.constrains('address_home_id')
    def validate_partner(self):
        """
        Validacion campo el partner de address_home_id solo se seleccione en un empleado
        """
        if self.address_home_id:
            records = self.env['hr.employee'].search([('address_home_id', '=', self.address_home_id.id)])
            if len(records) > 1:
                raise Warning(u"Solo se puede asociar un tercero por empleado, este no puede repetirse.")